$(document).ready(function () {
    var deleteButtons = $('.btn-delete');
    var modal = $('#deleteModal');
    var confirmDeleteButton = $('#confirmDelete');
    var modalBodyContent = $('#modalBodyContent');
    var cancelButton = $('#cancel');
    var deleteRow = null;
    var deleteEndpoint = null;

    deleteButtons.each(function () {
        $(this).on('click', function () {
            deleteEndpoint = $(this).data('url');
            var objectName = $(this).data('id');
            deleteRow = $(this).closest('tr'); // or $(this).closest('.card') for mobile view
            modalBodyContent.text(`Вы действительно хотите удалить "${objectName}"?`);
            confirmDeleteButton.show();
            cancelButton.text('Нет');
            modal.modal('show');
        });
    });

    confirmDeleteButton.on('click', function () {
        if (deleteEndpoint) {
            $.ajax({
                url: deleteEndpoint,
                type: 'DELETE',
                headers: {
                    'X-CSRFToken': getCookie('csrftoken')
                },
                success: function (response, status, xhr) {
                    if (xhr.status === 204) {
                        //modalBodyContent.text('Удаление прошло успешно');
                        modal.modal('hide');
                        deleteRow.remove();
                    } else {
                        modalBodyContent.text('Неизвестная ошибка');
                    }
                    confirmDeleteButton.hide();
                    cancelButton.text('Закрыть');
                },
                error: function (xhr) {
                    if (xhr.status === 400) {
                        var errors = JSON.parse(xhr.responseText);
                        modalBodyContent.text(errors[0]);
                    } else {
                        modalBodyContent.text('Неизвестная ошибка');
                    }
                    confirmDeleteButton.hide();
                    cancelButton.text('Закрыть');
                }
            });
        }
    });

    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = $.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
});
